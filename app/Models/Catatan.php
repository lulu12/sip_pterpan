<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Catatan extends Model
{
    protected $table = 'catatan';
    protected $fillable = ['id_catatan', 'nim', 'id_jadwal', 'catatanMhs'];
}
