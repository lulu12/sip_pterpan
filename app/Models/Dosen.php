<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Dosen extends Model
{
    protected $table = 'dosen';
    protected $fillable = ['nidn', 'namaDosen', 'noTelpDosen', 'emailDosen', 'passDosen', 'statusDosen'];
}
