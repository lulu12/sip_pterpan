<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Perwalian extends Model
{
    protected $table = 'perwalian';
    protected $fillable = ['id_jadwal', 'nidn', 'nim', 'kategori', 'keterangan', 'tanggalPerwalian', 'jamPerwalian', 'tanggalSetup', 'jamSetup', 'status'];
}
