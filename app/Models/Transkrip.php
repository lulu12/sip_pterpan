<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transkrip extends Model
{
    protected $table = 'transkrip';
    protected $fillable = ['id_transkrip', 'nim', 'hasil_transkrip'];
}
